
pub mod first;  // naive implementation
pub mod second; // list and iterators
pub mod third;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
