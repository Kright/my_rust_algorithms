// https://rust-unofficial.github.io/too-many-lists/second-iter-mut.html

pub struct List<T> {
    head: Link<T>
}

type Link<T> = Option<Box<Node<T>>>;

struct Node<T> {
    value: T,
    next: Link<T>,
}

pub struct IntoIter<T>(List<T>);

pub struct Iter<'a, T> {
    next: Option<&'a Node<T>>,
}

pub struct IterMut<'a, T> {
    next: Option<&'a mut Node<T>>,
}

impl<T> List<T> {
    pub fn new() -> Self {
        List { head: None }
    }

    pub fn push(&mut self, value: T) {
        let new_node = Box::new(Node {
            value,
            next: self.head.take(),
        });

        self.head = Some(new_node);
    }

    pub fn pop(&mut self) -> Option<T> {
        self.pop_node().map(|x| x.value)
    }

    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|boxed| &boxed.value)
    }

    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.head.as_mut().map(|boxed| &mut boxed.value)
    }

    pub fn into_iter(self) -> IntoIter<T> {
        IntoIter(self)
    }

    pub fn iter(&self) -> Iter<T> {
        Iter { next: self.head.as_ref().map(|boxed| &**boxed) }
    }

    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut { next: self.head.as_mut().map(|boxed| &mut **boxed) }
    }

    fn pop_node(&mut self) -> Link<T> {
        self.head.take().map(|mut node| {
            self.head = node.next.take();
            node
        })
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        while let Some(_) = self.pop_node() {}
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop()
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|node| {
            // self.next = node.next.as_ref().map(|node| &**node);
            self.next = node.next.as_ref().map::<&Node<T>, _>(|node|&node);
            &node.value
        })
    }
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node| {
            self.next = node.next.as_mut().map(|node| &mut **node);
            &mut node.value
        })
    }
}


mod test {
    use super::List;

    #[test]
    fn push_n_pop() {
        let mut list = List::new();

        list.push(1);
        list.push(2);

        let last = list.pop();

        let first = list.pop();
        let empty = list.pop();

        assert_eq!(last, Some(2));
        assert_eq!(first, Some(1));
        assert_eq!(empty, None);

        list.push(4);
        let f = list.pop();
        assert_eq!(f, Some(4));

        let empty = list.pop();
        assert_eq!(empty, None);
    }

    #[test]
    fn peek() {
        let mut list = List::new();
        assert_eq!(list.peek(), None);

        list.push(1);
        assert_eq!(list.peek(), Some(&1));

        list.push(2);
        assert_eq!(list.peek(), Some(&2));

        list.pop();
        assert_eq!(list.peek(), Some(&1));

        list.pop();
        assert_eq!(list.peek(), None);

        list.pop();
        assert_eq!(list.peek(), None);

        list.push(4);
        assert_eq!(list.peek(), Some(&4));
    }

    #[test]
    fn peek_mut() {
        let mut list = List::new();
        assert_eq!(list.peek_mut(), None);

        list.push(1);
        assert_eq!(list.peek_mut(), Some(&mut 1));
        list.peek_mut().map(|r| { *r = 42 });
        assert_eq!(list.peek_mut(), Some(&mut 42));

        list.push(2);
        assert_eq!(list.peek_mut(), Some(&mut 2));

        list.pop();
        assert_eq!(list.peek_mut(), Some(&mut 42));

        list.pop();
        assert_eq!(list.peek_mut(), None);

        list.pop();
        assert_eq!(list.peek_mut(), None);

        list.push(4);
        assert_eq!(list.peek_mut(), Some(&mut 4));
    }

    #[test]
    fn into_iter() {
        let mut list = List::new();
        list.push(1);
        list.push(2);

        let mut iter = list.into_iter();
        assert_eq!(iter.next(), Some(2));
        assert_eq!(iter.next(), Some(1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn iter() {
        let mut list = List::new();
        list.push(1); list.push(2);

        let mut iter = list.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn iter_mut() {
        let mut list = List::new();
        list.push(1); list.push(2); list.push(3);

        let mut iter = list.iter_mut();
        assert_eq!(iter.next(), Some(&mut 3));
        assert_eq!(iter.next(), Some(&mut 2));
        assert_eq!(iter.next(), Some(&mut 1));

        let mut iter = list.iter_mut();
        assert_eq!(iter.next(), Some(&mut 3));
        assert_eq!(iter.next(), Some(&mut 2));
        assert_eq!(iter.next(), Some(&mut 1));
    }
}
