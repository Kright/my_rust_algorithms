use std::mem;

#[derive(Debug)]
pub struct List {
    head: Link<i32>
}

type Link<T> = Option<Box<Node<T>>>;


impl List {
    pub fn new() -> Self {
        List { head: None }
    }

    pub fn push(&mut self, value: i32) {
        let new_node = Box::new(Node {
            value: value,
            next: mem::replace(&mut self.head, None),
        });

        self.head = Some(new_node);
    }

    pub fn pop(&mut self) -> Option<i32> {
        self.pop_node().map(|x| x.value)
    }

    fn pop_node(&mut self) -> Link<i32> {
        match mem::replace(&mut self.head, None) {
            None => None,
            Some(mut link) => {
                self.head = mem::replace(&mut link.next, None);
                Some(link)
            }
        }
    }
}

impl Drop for List {
    fn drop(&mut self) {
        while let Some(_) = self.pop_node() {
        }
    }
}


#[derive(Debug)]
struct Node<T> {
    value: T,
    next: Link<T>,
}

mod test {
    use super::List;

    #[test]
    fn basics() {
        let mut list = List::new();

        list.push(1);
        list.push(2);

        let last = list.pop();
        let first = list.pop();
        let empty = list.pop();

        assert_eq!(last, Some(2));
        assert_eq!(first, Some(1));
        assert_eq!(empty, None);
    }
}
